cluster_id<-function(x){
  #Fonction qui permet simplement de recuperer l'indice de la colonne des clusters dans un eventlog passe par actitrac
  if(!"eventlog"%in%class(x)){
    stop('Please provide object of class "eventlog".')
  }
  which(colnames(x)=="clusters")
}
