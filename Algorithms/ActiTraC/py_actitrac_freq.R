py_actitrac_freq<-function(L,nb_clus=2,tf=.97,mcs=5,N=TRUE,verbose=TRUE,only_dist=FALSE){
  
  library(pm4py)
  #
  #Fonction qui execute l'algorithme de clustering ActiTraC sur les traces d'un eventlog.
  #Cette version utilise une version de pm4py importee de python
  #Liste des parametres :
  #- L : log (data frame) ou eventlog genere par bupaR
  #- nb_clus : numerique / nombre de clusters a trouver
  #- tf : numerique / adequation minimum des modeles generes par actitrac pour decider de l'inclusion d'une trace a un cluster
  #- mcs : numerique / taille minimum des clusters
  #- N : booleen / TRUE : les traces ignorees par l'algorithme forment un groupe a part. FALSE : Les traces ignorees sont affectees au cluster
  #                       donnant l'adequation la plus elevee
  #- verbose : booleen / TRUE : l'algorithme imprime un suivi de chaque etape
  #
  #
  
  #Installation / chargement des packages necessaires
  
  using<-function(...){
    
    #fonction qui prend un vecteur de caracteres et verifie s'ils sont installes, les installe si non, puis les charge
    
    libs<-unlist(list(...))
    req<-unlist(lapply(libs,require,character.only=TRUE))
    need<-libs[req==FALSE]
    if(length(need)>0){ 
      install.packages(need)
      lapply(need,require,character.only=TRUE)
    }
  }
  
  using("magrittr","heuristicsmineR","dplyr","TraMineR","pm4py","data.table","cluster","pbapply","bupaR","processmapR")
  
  if(!"eventlog"%in%class(L)){
    id<-select.list(colnames(L),graphics=TRUE,title="Case identifier variable:")
    activity<-select.list(colnames(L)[-which(colnames(L)==id)],graphics=TRUE,title="Activity variable:")
    timestamp<-select.list(colnames(L)[-which(colnames(L)%in%c(id,activity))],graphics=TRUE,title="Timestamp variable:")
    if(verbose) cat("\n","Conversion to eventlog...","\n")
    l<-simple_eventlog(L,
                       id,
                       activity,
                       timestamp)
  }else{
    id<-case_id(L)
    activity<-activity_id(L)
    timestamp<-timestamp(L)
    l<-L
  }
  
  color_sup<-function(){
    rstudio_with_ansi_support <- function() {
      if (Sys.getenv("RSTUDIO", "") == "") return(FALSE)
      
      ## This is set *before* the rstudio initialization, in 1.1 and above
      if ((cols <- Sys.getenv("RSTUDIO_CONSOLE_COLOR", "")) != "" &&
          !is.na(as.numeric(cols))) {
        return(TRUE)
      }
      
      ## This only works if the initialization is complete
      requireNamespace("rstudioapi", quietly = TRUE) &&
        rstudioapi::isAvailable() &&
        rstudioapi::hasFun("getConsoleHasColor")
    }
    enabled <- getOption("crayon.enabled")
    if (!is.null(enabled)) {
      return(isTRUE(enabled))
    }
    if (rstudio_with_ansi_support() && sink.number() == 0) {
      return(TRUE)
    }
    if (!isatty(stdout())) {
      return(FALSE)
    }
    if (os_type() == "windows") {
      if (Sys.getenv("ConEmuANSI") == "ON") {
        return(TRUE)
      }
      if (Sys.getenv("CMDER_ROOT") != "") {
        return(TRUE)
      }
      return(FALSE)
    }
    if (inside_emacs() && !is.na(emacs_version()[1]) && emacs_version()[1] >= 
        23) {
      return(TRUE)
    }
    if ("COLORTERM" %in% names(Sys.getenv())) {
      return(TRUE)
    }
    if (Sys.getenv("TERM") == "dumb") {
      return(FALSE)
    }
    grepl("^screen|^xterm|^vt100|color|ansi|cygwin|linux", 
          Sys.getenv("TERM"), ignore.case = TRUE, perl = TRUE)
  } #Detecte si les couleurs sont supportees par la console
  
  if(color_sup()) using("crayon")
  
  crayon_is_loaded<-"crayon"%in%installed.packages()[,"Package"]
  
  
  ###### Initialisation
  
  
  if(verbose) cat("\n","INITIALIZATION","\n")
  
  if(verbose) cat("\n","Calculating traces frequency and ranks...","\n")
  
  GL<-traces(l)
  GL$rank_trace<-1:nrow(GL)
  cl<-case_list(l)
  GL$`case:concept:name`<-cl$`case:concept:name`[!duplicated(cl$trace)]
  GL<-GL[,c(5,1:4)]
  GL<-as.data.frame(GL)
  
  CS<-NULL
  R<-GL #dpi restants, pour l'instant aucun n'est traite
  
  cpt<-1 #Numero de cluster
  
  
  
  ###### Boucle while
  
  if(verbose) cat("\n","Starting clustering phase.","\n","\n")
  
  while(cpt<=nb_clus & nrow(R)!=0){
    
    C<-NULL #Ensemble des dpi dans le cluster
    I<-NULL #Ensemble des dpi ignores
    
    if(verbose) cat("###############################################################################################################","\n")
    if(verbose) cat("\n","CLUSTER",cpt,"\n")
    
    ##### Phase 1 : Selection
    
    if(verbose) cat("\n",
                    "___________________________________________________________________________","\n","\n",
                    "                               ","Selection","                             ","\n",
                    "___________________________________________________________________________","\n")
    
    repeat{
      
      if(is.null(I)){
        RI<-R
      }else RI<-R[-which(R[,id]%in%I[,id]),]
      
      if(verbose) cat("\n","Traces left for cluster",cpt,":",nrow(RI)," | ")
      
      cur_dpi<-RI[1,] #dpi considere s'il n'y a aucun dpi dans le cluster et qu'il n'y a qu'un dpi dans le top w% de RI
      
      pn<-pm4py$algo$discovery$heuristics$factory$apply(l[as.data.frame(l)[,id]%in%union(C[,id],cur_dpi[,id]),])
      
      if(is.null(C)) ev<-1
      else ev<-evaluation_fitness(l[as.data.frame(l)[,id]%in%union(C[,id],cur_dpi[,id]),],
                                  pn[[1]],
                                  pn[[2]],
                                  pn[[3]])$log_fitness

      if(verbose) cat("fitness :",format(round(ev,4),nsmall=4))
      
      if(ev>=tf){
        C<-rbind(C,cur_dpi)
        R<-R[-which(R[,id]==cur_dpi[,id]),]
      }
      else{
        if(verbose){
          if(crayon_is_loaded) cat(red(" -> Unfit"))
          else cat(" -> Unfit")
        }
        if(nrow(C)>=mcs){
          if(verbose) cat("\n","\n","Cluster",cpt,"reached at least required cluster size and current trace doesn't fit")
          if(verbose) cat("\n",
                          "___________________________________________________________________________","\n","\n",
                          "                             ","Look ahead","                              ","\n",
                          "___________________________________________________________________________","\n")
          if(verbose) cat("\n","Adding remaining fitting traces to cluster",cpt,"...")
          pn<-pm4py$algo$discovery$heuristics$factory$apply(l[as.data.frame(l)[,id]%in%C[,id],])
          
          ##### Phase 2 : Look ahead
          #La taille min de cluster a ete atteinte
          #On rajoute a C les traces dont l'adequation vaut 100%, le reste est ignore et sera teste pour le cluster suivant
          
          evbool<-pbsapply(1:nrow(R),
                           function(x) evaluation_fitness(l[as.data.frame(l)[,id]%in%R[x,id],],
                                                          pn[[1]],
                                                          pn[[2]],
                                                          pn[[3]])$perc_fit_traces)==100
          
          if(verbose) cat("\n",sum(evbool),"traces added to cluster",cpt,"\n")
          C<-rbind(C,R[evbool,])
          R<-R[evbool==FALSE,]
          break
        }
        else{
          if(verbose){
            if(crayon_is_loaded) cat(red(" -> Ignored"))
            else cat(" -> Ignored") 
          }
          I<-rbind(I,cur_dpi)
        }
      }
      if(nrow(R)==0 | sum(R[,id]%in%I[,id])==nrow(R)){
        if(verbose) cat("\n","cluster",cpt,"done","\n","\n")
        break
      }
    }
    C$clus<-cpt
    CS<-rbind(CS,C)
    if(verbose) cat("\n","Cluster",cpt,"done","\n","\n")
    cpt<-cpt+1
  }
  
  ##### Phase 3 : Residual traces resolution
  
  if(verbose) cat("###############################################################################################################","\n")
  
  if(verbose) cat("\n",
                  "___________________________________________________________________________","\n","\n",
                  "                     ","Residual trace resolution","                       ","\n",
                  "___________________________________________________________________________","\n")
  
  if(N){
    if(verbose) cat("\n",nrow(R),"ignored traces form a separate cluster of index 0","\n")
    if(nrow(R)!=0){
      R$clus<-0
      CS<-rbind(CS,R)
    }
  }
  else{
    if(nrow(R)!=0){
      if(verbose) cat("\n",nrow(R),"Ignored traces are affected to best fitting cluster...","\n","\n")
      
      mods<-lapply(sort(unique(CS$clus)),
                   function(x) pm4py$algo$discovery$heuristics$factory$apply(l[as.data.frame(l)[,id]%in%CS[CS$clus==x,id],]))
      
      ignored<-R[,id]
      
      fits<-sapply(1:length(mods),
                   function(y) pbsapply(ignored,
                                        evaluation_fitness(l[as.data.frame(l)[,id]==x,],
                                                           mods[[y]][[1]],
                                                           mods[[y]][[2]],
                                                           mods[[y]][[3]])$average_trace_fitness))%>%
        data.frame%>%
        setNames(1:length(mods))
      R$clus<-apply(fits,1,which.max)
      CS<-rbind(CS,R)
    }
    else{
      cat("\n","No traces left ignored.")
    }
  }
  
  if(verbose) cat("\n","Assigning duplicated traces to corresponding clusters...")
  dat_traces<-case_list(l)
  dat_traces$clus<-CS$clus[match(dat_traces$trace,CS$trace)]
  colnames(dat_traces)[1]<-id
  #
  vars<-c(case_id(l),activity_id(l),timestamp(l))
  #assigner le clustering a l'eventlog
  l$clusters<-dat_traces$clus[match(as.data.frame(l)[,vars[1]],as.data.frame(dat_traces)[,vars[1]])]
  #
  if(verbose) cat("Done.","\n")
  l
}
