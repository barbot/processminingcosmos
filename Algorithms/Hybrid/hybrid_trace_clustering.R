
hybrid_trace_clustering <- function(eventlog,number_of_clusters,treshold_f1score,minimun_clusters_size,neighbourhood_size,clustering_technique,distance_technique,discovery_technique,max_distance,verbose) {
  
  tracelist <- as.data.frame(traces(eventlog))
  tracelist$id <- c(1:nrow(tracelist))
  tracelist$cluster <- 0
  tracelist$flag <- 0
  tracelist$status <- FALSE
  
  eventlog <- trace_assignement(eventlog,tracelist)

  dt_empty <- data.frame(Case=character(), 
                         Activity=character(),
                         Timestamp=as.Date(character()),
                         Resource=character(),
                         Lifecycle=character(),
                         Instance=character())
  
  ev_empty <- as.data.frame(dt_empty) %>%
    rename(`case:concept:name`=Case,
           `concept:name`=Activity) %>%
    eventlog(case_id="case:concept:name",
             activity_id="concept:name",
             timestamp="Timestamp",
             resource_id ="Resource",
             lifecycle_id="Lifecycle",
             activity_instance_id="Instance")
  
  ev_empty$`concept:name` <- as.character(ev_empty$`concept:name`)
  ev_empty$Resource <- as.character(ev_empty$Resource)
  ev_empty$Lifecycle <- as.character(ev_empty$Lifecycle)
  ev_empty$trace <- 0
  
  clusters <- list()
  for (c in 1:number_of_clusters) {
    ev <- list(ev=ev_empty)
    clusters <- append(clusters,ev)
  }
  
  cl <- 1
  
  result <- main(eventlog,clusters,tracelist,cl,number_of_clusters,treshold_f1score,minimun_clusters_size,neighbourhood_size,clustering_technique,distance_technique,discovery_technique,max_distance,verbose)
  
  return(result)
  
}
