
evaluation <- function(cluster) {
  pn <- pm4py$algo$discovery$heuristics$factory$apply(cluster)
  fitness <- evaluation_fitness(cluster,pn[[1]],pn[[2]],pn[[3]])$log_fitness
  pft <- evaluation_fitness(cluster,pn[[1]],pn[[2]],pn[[3]])$perc_fit_traces
  precision <- evaluation_precision(cluster,pn[[1]],pn[[2]],pn[[3]])
  if (fitness + precision == 0) { f1score = 0 }
  else { f1score = 2 * (fitness * precision) / (fitness + precision) }
  eval <- list(fitness=fitness,pft=pft,precision=precision,f1score=f1score)
  return(eval)
}
