
########################################################################################

Command line:	Cosmos bpi0.gspn lha.lha --max-run 100000
Model path:	bpi0.gspn
LHA path:	lha.lha
Seed:	952960001
:
Estimated value:	1.23615577671247
Confidence interval:	[1.22774412818221 , 1.24456742524273]
Minimal and maximal value:	[0.00409370082836096 , 12.417368129626]
Width:	0.016823297060514
Level:	0.99
Method:	Confidence interval computed using approximation to normal low.
Confidence level:	0.99
Total paths:	100000
Accepted paths:	100000
Batch size:	1000
Time for simulation:	0.201856797s
Total CPU time:	1.124935
Total Memory used:	99.02 MB
Number of jobs:	1

########################################################################################

tm = 60.5
rmse = 180

########################################################################################

fitness = 0.997
precision = 0.854
f1-score = 0.92

########################################################################################

