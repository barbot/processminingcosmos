
########################################################################################

Command line:	Cosmos hospital0.gspn lha.lha --max-run 100000
Model path:	hospital0.gspn
LHA path:	lha.lha
Seed:	2957062127
:
Estimated value:	3.1417270844185
Confidence interval:	[3.12915073842541 , 3.15430343041159]
Minimal and maximal value:	[0.203947868291303 , 14.7849916521709]
Width:	0.0251526919861833
Level:	0.99
Method:	Confidence interval computed using approximation to normal low.
Confidence level:	0.99
Total paths:	100000
Accepted paths:	100000
Batch size:	1000
Time for simulation:	0.158149014s
Total CPU time:	1.085305
Total Memory used:	99.26 MB
Number of jobs:	1

########################################################################################

tm = 10.1
rmse = 8.17

########################################################################################

fitness = 0.622
precision = 0.143
f1-score = 0.232

########################################################################################

